$(document).ready(function () {
    $.getJSON("./data/skills.json", function (skills) {
        // Iterate skills
        skills.forEach( function (skill) {
            var ownIcon = (skill.ownIcon);
            if(ownIcon=="true"){
                $('#skills-container').append(
                    `
                    <div class="col-4 text-center skill">
                        <img class="hvr-grow ownIcon-5x" src="./assets/skills/${skill.icon}.svg" data-toggle="tooltip" placement="top" title="${skill.tooltip}"/>
                        <p>
                            <span class="badge text-center">${skill.name}</span>
                        </p>
                    </div>
                    `
                );
            }
            else{
                $('#skills-container').append(
                    `
                    <div class="col-4 text-center skill">
                        <i class="hvr-grow fa-5x devicon-${skill.icon}" data-toggle="tooltip" placement="top" title="${skill.tooltip}"></i>
                        <p>
                            <span class="badge text-center">${skill.name}</span>
                        </p>
                    </div>
                    `
                );
            }
        });

        //Content loaded
        $('#skills-preloader').remove();
        
        // Activate skill tooltips
        $('[data-toggle="tooltip"]').tooltip();
    });
})