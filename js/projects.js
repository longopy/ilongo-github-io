$(document).ready(function () {
    $.getJSON('./data/projects.json', function (projects) {

        // Iterate projects
        projects.forEach(function (project, index) {

            var container = (project.collaboration === 'true')
                ? $('#contributions-container')
                : $('#projects-container');

            var status = (project.status === 'success' )
                ? 'success'
                : 'danger';
            var status_text = (project.status === 'success')
                ? 'Release'
                : 'In progress';

            var project_repository = (project.repository === 'private')
                ? 'PRIVATE <i class="fas fa-lock text-danger"></i>'
                : 'PUBLIC <i class="fas fa-unlock text-success"></i>';
            
            container.append(
                `
                <div class="sub-container card border border-light mb-5">
                    <div class="card-body">
        
                        <!-- Text info -->
                        <div class='row'>
                            <div class='col-sm-8'>
                                <!-- Project name -->
                                <h4 class="card-title">${project.name}
                                    <span class="text-white badge badge-${status}"> ${status_text} </span>
                                </h4>
                            </div>
                            <div class='col-sm-4'>
                                <!-- Latest version -->
                                <h5 class='text-right'>
                                  <span class="pill badge badge-pill badge-light"> ${project_repository}</span> 
                                </h5>
                                <!-- Latest modified -->
                                <h6 class='text-right'>
                                    Latest on &nbsp;<span class="pill badge badge-pill badge-secondary"> ${project.last_update} </span> 
                                </h6>
                            </div>
                        </div>

                        <!-- Project subtitle -->
                        <h6 class="card-subtitle mb-2 text-muted">${project.subtitle}</h6>

                        <!-- Project description -->
                        <p class="card-text">${project.description}</p>
        
                        <!-- Technologies containers -->
                        <div id="tech-container-${index}" class="row"></div>

                        <br>

                        <div class="row justify-content-end">
                        
                            <!-- GitHub link -->
                            <a href="${project.github_repo}" role="button" target="_blank" class="hvr-grow btn btn-outline-dark project-link" `+(project.github_repo=="disabled" ? 'hidden': '')+`>GitHub repository <i class="fab fa-github"></i></a>
                                                     
                            <!-- View link -->
                           <a href="${project.web}" role="button" target="_blank" class="hvr-grow btn btn-outline-dark project-link"  `+(project.web=="disabled" ? 'hidden': '')+`>View <i class="fas fa-eye"></i></a>
                            
                        </div>
        
                    </div>
                </div>
                `
            );

            // Iterate technologies icons
            project.tech_icons.forEach(function (techicon) {
                if(techicon.ownIcon==="false"){
                    //Inject tech icon
                $(`#tech-container-${index}`).append(
                    `
                    <div class="col text-center">
                        <i class="m-1 fa-3x devicon-${techicon.icon}"></i>
                    </div>
                    `
                );
                }
                else{
                    $(`#tech-container-${index}`).append(
                        `
                        <div class="col text-center">
                        <img class="m-1 ownIcon-3x" src="./assets/skills/${techicon.icon}.svg"/>
                        </div>
                        `
                    );
                }
            });

            //Content loaded
            $('#projects-preloader').remove();
            $('#contributions-preloader').remove();
        })
    });
})