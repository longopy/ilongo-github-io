# Personal Web

This is my personal web repository.

I coded it with a dynamic system that load the information from json files.

# Structure project

- **index.html**: load js components
- **js**: contains javascript components
- **utils**: contains utilities (dependencies) used in project
- **data**: contains data consumed by .js files
- **assets**: contains images, acreditations, icons and other resorces
- **css**: contains project 


# Dependences

- Bootstrap 4
- Devicons
- FontAwesome
- JQuery
- github-calendar :calendar: Thanks to @IonicaBizau
